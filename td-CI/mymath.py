
def add(x,y):
    """
    docstring
    """
    return x+y

def substract(x,y):
    return x-y

def multiply(x,y):
    return x*y

def divide(x,y):
    if y == 0:
        raise ValueError('division par O impossible')
    else:
        return x/y
