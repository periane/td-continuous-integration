import unittest

import mymath


class TestSum(unittest.TestCase):

    def test_add(self):
        """
        Vérifie que l'addition fonctionne correctement
        et ce pour les différents types
        """
        self.assertEqual(mymath.add(1,2), 3)
        self.assertEqual(mymath.add(2,1), 3)
        self.assertEqual(mymath.add(5.7,2), 7.7)
        self.assertEqual(mymath.add(3.7,2.3), 6.0)

    def test_divide(self):
        """
        Vérifie que la division fonctionne
        """
        self.assertEqual(mymath.divide(4,2), 2)

        self.assertRaises(ValueError, mymath.divide, 4, 0)
        # une autre manière d'écrire cette instruction
        with self.assertRaises(ValueError):
            mymath.divide(4, 0)

if __name__ == '__main__':
    unittest.main()
