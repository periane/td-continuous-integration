from datetime import datetime

def get_time_of_day():

    now = datetime.now()
    time = int(now.strftime("%H"))

    if (time >= 0 and time < 6):
        return "Bonne nuit"
    if (time >= 6 and time < 12):
        return "Café du matin"
    if (time >= 12 and time < 18):
        return "Bon après midi"

    return "Bonne soirée"

print (get_time_of_day())
