import unittest

import time_of_day


class TestTimeOfDay(unittest.TestCase):


    def test_time_of_day(self):
        """
        Vérifie que la fonction get_time_of_day fonctionne
        et ce pour toutes les heures de la journée
        """
        self.assertEqual(time_of_day.get_time_of_day(12), "Bon apres midi")
        self.assertEqual(time_of_day.get_time_of_day(5), "Bonne nuit")
        self.assertEqual(time_of_day.get_time_of_day(20), "Bonne soiree")
        self.assertEqual(time_of_day.get_time_of_day(10), "Cafe du matin")
        self.assertEqual(time_of_day.get_time_of_day(24), "Bonne soiree")

        with self.assertRaises(TypeError):
            time_of_day.get_time_of_day('a')


if __name__ == '__main__':
    unittest.main()
